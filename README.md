# Links

Miro:<br>
https://miro.com/app/board/uXjVMUdos_8=/<br>
Wachtwoord: fieldlab<br>
<br>
<br>
Gitlab repository voor Archimate files:<br>
https://gitlab.com/vng-realisatie/architectuur/zandbak/bronnen/
<br>
<br>
Gemeentelijk Gegevensmodel (GGM):<br>
https://gemeente-delft.github.io/Gemeentelijk-Gegevensmodel/
<br>
<br>
Document met aantekeningen:<br>
https://gitlab.com/commonground/docs/fieldlabs/-/raw/master/aantekeningen_dag_1.docx?inline=false


